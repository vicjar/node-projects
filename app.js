var express =  require('express');
var app = express();
var bodyParser = require('body-parser');
var user = require('./controllers/user');
var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var db;

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'pug');

/*MongoClient.connect('mongodb://vicjar:1994619775830vj@ds147487.mlab.com:47487/crud', (err, database) => {
  if (err) return console.log(err);
  db = database;
  app.listen(3000, () => {
    console.log('listening on 3000');
  });
});*/
mongoose.connect('mongodb://vicjar:1994619775830vj@ds147487.mlab.com:47487/crud', (err, database) => {
  if (err) return console.log(err);
  db = database;
  app.listen(3000, () => {
    console.log('listening on 3000');
  });
});

app.get('/', (req, res) => {
	res.render('index');
});
app.get('/insertado',(req,res) => {
	res.send('valor guardado');
});
app.get('/search', (req, res) => {
  //user.read(req,db,res);
  //res.redirect('/users'); 
  user.read(db, req, res);
});
app.post('/create', (req, res) => {
  user.add(req,db);
  res.redirect('/insertado');
});
